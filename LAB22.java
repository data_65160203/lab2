public class LAB22 {

    public static void main(String[] args) {
        int[] nums1 = {3, 2, 2, 3};
        int val1 = 3;
        int result1 = removeElement(nums1, val1);
        System.out.print("Output: " + result1 + ", nums = [");
        for (int i = 0; i < result1; i++) {
            System.out.print(nums1[i] + (i == result1 - 1 ? "" : ", "));
        }
        System.out.println("]");

        int[] nums2 = {0, 1, 2, 2, 3, 0, 4, 2};
        int val2 = 2;
        int result2 = removeElement(nums2, val2);
        
        System.out.print("Output: " + result2 + ", nums = [");
        for (int i = 0; i < result2; i++) {
            System.out.print(nums2[i] + (i == result2 - 1 ? "" : ", "));
        }
        System.out.println("]");
        
    }

    private static int removeElement(int[] nums, int val) {
        int k = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[k] = nums[i];
                k++;
            }
        }

        return k;
    }
}
